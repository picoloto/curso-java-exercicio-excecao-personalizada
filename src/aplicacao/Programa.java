package aplicacao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

import modelo.entidades.Reserva;
import modelo.excecoes.DominioExcecao;

public class Programa {

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Scanner sc = new Scanner(System.in);

		try {
			System.out.print("N�mero do quarto: ");
			int quarto = sc.nextInt();

			System.out.print("Data do check-in (DD/MM/YYYY): ");
			Date checkIn = sdf.parse(sc.next());

			System.out.print("Data do check-out (DD/MM/YYYY): ");
			Date checkOut = sdf.parse(sc.next());

			Reserva reserva = new Reserva(quarto, checkIn, checkOut);
			System.out.println("Reserva: " + reserva);

			System.out.println();
			System.out.println("Dados para atualiza��o da reserva");

			System.out.print("Data do check-in (DD/MM/YYYY): ");
			checkIn = sdf.parse(sc.next());

			System.out.print("Data do check-out (DD/MM/YYYY): ");
			checkOut = sdf.parse(sc.next());

			reserva.atualizaDatas(checkIn, checkOut);
			System.out.println("Reserva: " + reserva);
		}
		catch (RuntimeException e) {
			System.out.println("Erro inesperado");
		}catch (ParseException e) {
			System.out.println("Formato de data invalido");
		} catch (DominioExcecao e) {
			System.out.println(e.getMessage());
		}

		sc.close();
	}
}
