package modelo.entidades;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import modelo.excecoes.DominioExcecao;

public class Reserva {
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	private Integer quarto;
	private Date checkIn;
	private Date checkOut;

	public Reserva() {
	}

	public Reserva(Integer quarto, Date checkIn, Date checkOut) throws DominioExcecao {
		if (!checkOut.after(checkIn)) {
			throw new DominioExcecao("Erro na reserva: Data de check-out deve ser posterior a data de check-in");
		}
		
		this.quarto = quarto;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
	}

	public Integer getQuarto() {
		return quarto;
	}

	public void setQuarto(Integer quarto) {
		this.quarto = quarto;
	}

	public Date getCheckIn() {
		return checkIn;
	}

	public Date getCheckOut() {
		return checkOut;
	}

	public long duracao() {
		long diferenca = checkOut.getTime() - checkIn.getTime();

		return TimeUnit.DAYS.convert(diferenca, TimeUnit.MILLISECONDS);
	}

	public void atualizaDatas(Date checkIn, Date checkOut) throws DominioExcecao {
		Date agora = new Date();

		if (checkIn.before(agora) || checkOut.before(agora)) {
			throw new DominioExcecao("Erro na reserva: Datas de reserva para atualização devem ser futuras");
		}
		if (!checkOut.after(checkIn)) {
			throw new DominioExcecao("Erro na reserva: Data de check-out deve ser posterior a data de check-in");
		}

		this.checkIn = checkIn;
		this.checkOut = checkOut;
	}

	@Override
	public String toString() {
		return "Quarto " + quarto + ", checkIn: " + sdf.format(checkIn) + ", checkOut: " + sdf.format(checkOut) + ", "
				+ duracao() + " noites";
	}

}
